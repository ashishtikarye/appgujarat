package com.gujeducation.gujaratedu.Adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.gujeducation.R;
import com.gujeducation.gujaratedu.Activity.PdfScreen;
import com.gujeducation.gujaratedu.Activity.TestScreenSample;
import com.gujeducation.gujaratedu.Activity.VideoListChapterScreen;
import com.gujeducation.gujaratedu.Helper.Connection;
import com.gujeducation.gujaratedu.Helper.Functions;
import com.gujeducation.gujaratedu.Helper.ProgressLoadingView;
import com.gujeducation.gujaratedu.Interface.OnResult;
import com.gujeducation.gujaratedu.ServerAPIs.APIs;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChapterOptionAdapter extends RecyclerView.Adapter<ChapterOptionAdapter.MyViewHolder> implements OnResult {

    private final AppCompatActivity mainActivity;
    private final FragmentActivity activity;
    public boolean error = false;
    public InterstitialAd interstitialAd;
    ArrayList<String> listOption = new ArrayList();
    Functions mFunction;
    Intent intent;
    String chapterName, PDF, PDFName;
    ProgressLoadingView mView = null;
    private int mNumColumns = 0;
    private String standard;


    public ChapterOptionAdapter(AppCompatActivity mainActivity, FragmentActivity activity, String chapterName, ArrayList<String> listOption) {
        this.mainActivity = mainActivity;
        this.activity = activity;
        this.chapterName = chapterName;
        this.listOption = listOption;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup holder, int i) {
        View itemView = LayoutInflater.from(holder.getContext()).inflate(R.layout.row_chapter_option, holder, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MyViewHolder menuItemHolder = holder;
        mFunction = new Functions(activity);

        mView = new ProgressLoadingView();
        interstitialAd = new InterstitialAd(activity);
        interstitialAd.setAdUnitId("ca-app-pub-6923797063551368/3272831029");
        List<String> testDeviceIds = new ArrayList<String>();
        testDeviceIds.add("8A898BC8824C996E9320D350D4AF1F10");
        testDeviceIds.add("F3CBCC502BD4E653B8B534D769BC587A");
        testDeviceIds.add("FFB848305EE41D5DB1D6C522BFB75BEE");
        testDeviceIds.add("105122E1816DB58B97D2DF2E357E7A37");
        testDeviceIds.add("ED6E76F6E947CC1B01B01524B255999E");

        RequestConfiguration configuration =
                new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
        MobileAds.setRequestConfiguration(configuration);

        //final String sectionList = listSection.get(position);

        /*Log.e("ChpOptnAdp_Pref", "mediumId->" + mFunction.getPrefMediumId() +
                "\ngetSection->" + mFunction.getSection() +
                "\ngetSemester->" + mFunction.getSemester() +
                "\ngetStandardId->" + mFunction.getStandardId() +
                "\ngetSubjectId->" + mFunction.getSubjectId() +
                "\ngetChapterId->" + mFunction.getChapterId());*/


        try {
            if (listOption.get(position).equalsIgnoreCase("Textbook"))
                menuItemHolder.ivOption.setBackgroundResource(R.drawable.ic_read);
            if (listOption.get(position).equalsIgnoreCase("Exercise"))
                menuItemHolder.ivOption.setBackgroundResource(R.drawable.ic_exercise);
            if (listOption.get(position).equalsIgnoreCase("Worksheet"))
                menuItemHolder.ivOption.setBackgroundResource(R.drawable.ic_worksheet);
            if (listOption.get(position).equalsIgnoreCase("Videos"))
                menuItemHolder.ivOption.setBackgroundResource(R.drawable.ic_video);
            if (listOption.get(position).equalsIgnoreCase("Teacher Edition"))
                menuItemHolder.ivOption.setBackgroundResource(R.drawable.ic_teacher_edition);
            if (listOption.get(position).equalsIgnoreCase("MCQs"))
                menuItemHolder.ivOption.setBackgroundResource(R.drawable.ic_mcq);

            menuItemHolder.tvOption.setText(listOption.get(position));
            menuItemHolder.rlOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listOption.get(position).equalsIgnoreCase("Textbook")) {
                        loadInterstitialAd();
                        mView.show(activity.getSupportFragmentManager(), "load");
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdLoaded() {
                                // Showing a simple Toast message to user when an ad is loaded
                                //Toast.makeText(activity, "Interstitial Ad is Loaded", Toast.LENGTH_LONG).show();
                                showInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToLoad(LoadAdError adError) {
                                Log.e("loadFail", "error-" + adError);
                                if (mView != null)
                                    mView.dismiss();
                                // Showing a simple Toast message to user when and ad is failed to load

                                if (Functions.knowInternetOn(activity)) {
                                    APIs.getPDFChapter(mainActivity, ChapterOptionAdapter.this::onResult, mFunction.getPrefMediumId(),
                                            mFunction.getSection(), mFunction.getSemester(),
                                            mFunction.getStandardId(), mFunction.getSubjectId(),
                                            mFunction.getChapterId(), "Textbook");
                                } else
                                    Functions.showInternetAlert(activity);
                            }

                            @Override
                            public void onAdOpened() {
                                // Showing a simple Toast message to user when an ad opens and overlay and covers the device screen
                                //Toast.makeText(activity, "Interstitial Ad Opened", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClicked() {
                                // Showing a simple Toast message to user when a user clicked the ad
                                //Toast.makeText(activity, "Interstitial Ad Clicked", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdLeftApplication() {
                                // Showing a simple Toast message to user when the user left the application
                                //Toast.makeText(activity, "Interstitial Ad Left the Application", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClosed() {
                                //loading new interstitialAd when the ad is closed
                                // ((NewsCircularScreen) activity).loadInterstitialAd();
                                // Showing a simple Toast message to user when the user interacted with ad and got the other app and then return to the app again
                                //mView.show(activity.getSupportFragmentManager(), "load");
                                //Toast.makeText(activity, "Interstitial Ad is Closed", Toast.LENGTH_LONG).show();
                                if (mView != null)
                                    mView.dismiss();
                                if (Functions.knowInternetOn(activity)) {
                                    APIs.getPDFChapter(mainActivity, ChapterOptionAdapter.this::onResult, mFunction.getPrefMediumId(),
                                            mFunction.getSection(), mFunction.getSemester(),
                                            mFunction.getStandardId(), mFunction.getSubjectId(),
                                            mFunction.getChapterId(), "Textbook");
                                } else
                                    Functions.showInternetAlert(activity);

                                //Toast.makeText(activity, ""+newsList.getNewsCircularId(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    if (listOption.get(position).equalsIgnoreCase("Worksheet")) {

                        loadInterstitialAd();
                        mView.show(activity.getSupportFragmentManager(), "load");
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdLoaded() {
                                // Showing a simple Toast message to user when an ad is loaded
                                //Toast.makeText(activity, "Interstitial Ad is Loaded", Toast.LENGTH_LONG).show();
                                showInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToLoad(LoadAdError adError) {
                                Log.e("loadFail", "error-" + adError);
                                if (mView != null)
                                    mView.dismiss();
                                // Showing a simple Toast message to user when and ad is failed to load

                                if (Functions.knowInternetOn(activity)) {
                                    //((ChapterScreen)activity).showInterstitialAd();
                                    APIs.getPDFChapter(mainActivity, ChapterOptionAdapter.this::onResult, mFunction.getPrefMediumId(),
                                            mFunction.getSection(), mFunction.getSemester(),
                                            mFunction.getStandardId(), mFunction.getSubjectId(),
                                            mFunction.getChapterId(), "Worksheet");
                                } else
                                    Functions.showInternetAlert(activity);
                            }

                            @Override
                            public void onAdOpened() {
                                // Showing a simple Toast message to user when an ad opens and overlay and covers the device screen
                                //Toast.makeText(activity, "Interstitial Ad Opened", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClicked() {
                                // Showing a simple Toast message to user when a user clicked the ad
                                //Toast.makeText(activity, "Interstitial Ad Clicked", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdLeftApplication() {
                                // Showing a simple Toast message to user when the user left the application
                                //Toast.makeText(activity, "Interstitial Ad Left the Application", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClosed() {
                                //loading new interstitialAd when the ad is closed
                                // ((NewsCircularScreen) activity).loadInterstitialAd();
                                // Showing a simple Toast message to user when the user interacted with ad and got the other app and then return to the app again
                                //mView.show(activity.getSupportFragmentManager(), "load");
                                //Toast.makeText(activity, "Interstitial Ad is Closed", Toast.LENGTH_LONG).show();
                                if (mView != null)
                                    mView.dismiss();
                                if (Functions.knowInternetOn(activity)) {
                                    //((ChapterScreen)activity).showInterstitialAd();
                                    APIs.getPDFChapter(mainActivity, ChapterOptionAdapter.this::onResult, mFunction.getPrefMediumId(),
                                            mFunction.getSection(), mFunction.getSemester(),
                                            mFunction.getStandardId(), mFunction.getSubjectId(),
                                            mFunction.getChapterId(), "Worksheet");
                                } else
                                    Functions.showInternetAlert(activity);

                                //Toast.makeText(activity, ""+newsList.getNewsCircularId(), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }

                    if (listOption.get(position).equalsIgnoreCase("Exercise")) {
                        loadInterstitialAd();
                        mView.show(activity.getSupportFragmentManager(), "load");
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdLoaded() {
                                // Showing a simple Toast message to user when an ad is loaded
                                //Toast.makeText(activity, "Interstitial Ad is Loaded", Toast.LENGTH_LONG).show();
                                showInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToLoad(LoadAdError adError) {
                                Log.e("loadFail", "error-" + adError);
                                if (mView != null)
                                    mView.dismiss();
                                // Showing a simple Toast message to user when and ad is failed to load

                                if (Functions.knowInternetOn(activity)) {
                                    //((ChapterScreen)activity).showInterstitialAd();
                                    APIs.getPDFChapter(mainActivity, ChapterOptionAdapter.this::onResult, mFunction.getPrefMediumId(),
                                            mFunction.getSection(), mFunction.getSemester(),
                                            mFunction.getStandardId(), mFunction.getSubjectId(),
                                            mFunction.getChapterId(), "Exercise");
                                } else
                                    Functions.showInternetAlert(activity);
                            }

                            @Override
                            public void onAdOpened() {
                                // Showing a simple Toast message to user when an ad opens and overlay and covers the device screen
                                //Toast.makeText(activity, "Interstitial Ad Opened", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClicked() {
                                // Showing a simple Toast message to user when a user clicked the ad
                                //Toast.makeText(activity, "Interstitial Ad Clicked", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdLeftApplication() {
                                // Showing a simple Toast message to user when the user left the application
                                //Toast.makeText(activity, "Interstitial Ad Left the Application", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClosed() {
                                //loading new interstitialAd when the ad is closed
                                // ((NewsCircularScreen) activity).loadInterstitialAd();
                                // Showing a simple Toast message to user when the user interacted with ad and got the other app and then return to the app again
                                //mView.show(activity.getSupportFragmentManager(), "load");
                                //Toast.makeText(activity, "Interstitial Ad is Closed", Toast.LENGTH_LONG).show();
                                if (mView != null)
                                    mView.dismiss();
                                if (Functions.knowInternetOn(activity)) {
                                    //((ChapterScreen)activity).showInterstitialAd();
                                    APIs.getPDFChapter(mainActivity, ChapterOptionAdapter.this::onResult, mFunction.getPrefMediumId(),
                                            mFunction.getSection(), mFunction.getSemester(),
                                            mFunction.getStandardId(), mFunction.getSubjectId(),
                                            mFunction.getChapterId(), "Exercise");
                                } else
                                    Functions.showInternetAlert(activity);

                                //Toast.makeText(activity, ""+newsList.getNewsCircularId(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    if (listOption.get(position).equalsIgnoreCase("Teacher Edition")) {

                        loadInterstitialAd();
                        mView.show(activity.getSupportFragmentManager(), "load");
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdLoaded() {
                                // Showing a simple Toast message to user when an ad is loaded
                                //Toast.makeText(activity, "Interstitial Ad is Loaded", Toast.LENGTH_LONG).show();
                                showInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToLoad(LoadAdError adError) {
                                Log.e("loadFail", "error-" + adError);
                                if (mView != null)
                                    mView.dismiss();
                                // Showing a simple Toast message to user when and ad is failed to load

                                if (Functions.knowInternetOn(activity)) {
                                    //((ChapterScreen)activity).showInterstitialAd();
                                    APIs.getPDFChapter(mainActivity, ChapterOptionAdapter.this::onResult, mFunction.getPrefMediumId(),
                                            mFunction.getSection(), mFunction.getSemester(),
                                            mFunction.getStandardId(), mFunction.getSubjectId(),
                                            mFunction.getChapterId(), "Teacher Edition");
                                } else
                                    Functions.showInternetAlert(activity);
                            }

                            @Override
                            public void onAdOpened() {
                                // Showing a simple Toast message to user when an ad opens and overlay and covers the device screen
                                //Toast.makeText(activity, "Interstitial Ad Opened", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClicked() {
                                // Showing a simple Toast message to user when a user clicked the ad
                                //Toast.makeText(activity, "Interstitial Ad Clicked", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdLeftApplication() {
                                // Showing a simple Toast message to user when the user left the application
                                //Toast.makeText(activity, "Interstitial Ad Left the Application", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClosed() {
                                //loading new interstitialAd when the ad is closed
                                // ((NewsCircularScreen) activity).loadInterstitialAd();
                                // Showing a simple Toast message to user when the user interacted with ad and got the other app and then return to the app again
                                //mView.show(activity.getSupportFragmentManager(), "load");
                                //Toast.makeText(activity, "Interstitial Ad is Closed", Toast.LENGTH_LONG).show();
                                if (mView != null)
                                    mView.dismiss();
                                if (Functions.knowInternetOn(activity)) {
                                    //((ChapterScreen)activity).showInterstitialAd();
                                    APIs.getPDFChapter(mainActivity, ChapterOptionAdapter.this::onResult, mFunction.getPrefMediumId(),
                                            mFunction.getSection(), mFunction.getSemester(),
                                            mFunction.getStandardId(), mFunction.getSubjectId(),
                                            mFunction.getChapterId(), "Teacher Edition");
                                } else
                                    Functions.showInternetAlert(activity);

                                //Toast.makeText(activity, ""+newsList.getNewsCircularId(), Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                    if (listOption.get(position).equalsIgnoreCase("Videos")) {

                        loadInterstitialAd();
                        mView.show(activity.getSupportFragmentManager(), "load");
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdLoaded() {
                                // Showing a simple Toast message to user when an ad is loaded
                                //Toast.makeText(activity, "Interstitial Ad is Loaded", Toast.LENGTH_LONG).show();
                                showInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToLoad(LoadAdError adError) {
                                Log.e("loadFail", "error-" + adError);
                                if (mView != null)
                                    mView.dismiss();
                                // Showing a simple Toast message to user when and ad is failed to load

                                if (Functions.knowInternetOn(activity)) {
                                    //((ChapterScreen)activity).showInterstitialAd();
                                    intent = new Intent(activity, VideoListChapterScreen.class);
                                    intent.putExtra("section", mFunction.getSection());
                                    intent.putExtra("semester", mFunction.getSemester());
                                    intent.putExtra("standardId", mFunction.getStandardId());
                                    intent.putExtra("subjectId", mFunction.getSubjectId());
                                    intent.putExtra("chapterId", mFunction.getChapterId());
                                    activity.startActivity(intent);
                                } else
                                    Functions.showInternetAlert(activity);
                            }

                            @Override
                            public void onAdOpened() {
                                // Showing a simple Toast message to user when an ad opens and overlay and covers the device screen
                                //Toast.makeText(activity, "Interstitial Ad Opened", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClicked() {
                                // Showing a simple Toast message to user when a user clicked the ad
                                //Toast.makeText(activity, "Interstitial Ad Clicked", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdLeftApplication() {
                                // Showing a simple Toast message to user when the user left the application
                                //Toast.makeText(activity, "Interstitial Ad Left the Application", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClosed() {
                                //loading new interstitialAd when the ad is closed
                                // ((NewsCircularScreen) activity).loadInterstitialAd();
                                // Showing a simple Toast message to user when the user interacted with ad and got the other app and then return to the app again
                                //mView.show(activity.getSupportFragmentManager(), "load");
                                //Toast.makeText(activity, "Interstitial Ad is Closed", Toast.LENGTH_LONG).show();
                                if (mView != null)
                                    mView.dismiss();
                                if (Functions.knowInternetOn(activity)) {
                                    //((ChapterScreen)activity).showInterstitialAd();
                                    intent = new Intent(activity, VideoListChapterScreen.class);
                                    intent.putExtra("section", mFunction.getSection());
                                    intent.putExtra("semester", mFunction.getSemester());
                                    intent.putExtra("standardId", mFunction.getStandardId());
                                    intent.putExtra("subjectId", mFunction.getSubjectId());
                                    intent.putExtra("chapterId", mFunction.getChapterId());
                                    activity.startActivity(intent);
                                } else
                                    Functions.showInternetAlert(activity);

                                //Toast.makeText(activity, ""+newsList.getNewsCircularId(), Toast.LENGTH_SHORT).show();
                            }
                        });


                    }

                    if (listOption.get(position).equalsIgnoreCase("MCQs")) {
                        intent = new Intent(activity, TestScreenSample.class);
                        activity.startActivity(intent);
                        /*loadInterstitialAd();
                        mView.show(activity.getSupportFragmentManager(), "load");
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdLoaded() {
                                // Showing a simple Toast message to user when an ad is loaded
                                //Toast.makeText(activity, "Interstitial Ad is Loaded", Toast.LENGTH_LONG).show();
                                showInterstitialAd();
                            }

                            @Override
                            public void onAdFailedToLoad(LoadAdError adError) {
                                Log.e("loadFail", "error-" + adError);
                                if (mView != null)
                                    mView.dismiss();
                                // Showing a simple Toast message to user when and ad is failed to load

                                intent = new Intent(activity, TestScreenSample.class);
                                activity.startActivity(intent);
                            }

                            @Override
                            public void onAdOpened() {
                                // Showing a simple Toast message to user when an ad opens and overlay and covers the device screen
                                //Toast.makeText(activity, "Interstitial Ad Opened", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClicked() {
                                // Showing a simple Toast message to user when a user clicked the ad
                                //Toast.makeText(activity, "Interstitial Ad Clicked", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdLeftApplication() {
                                // Showing a simple Toast message to user when the user left the application
                                //Toast.makeText(activity, "Interstitial Ad Left the Application", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onAdClosed() {
                                //loading new interstitialAd when the ad is closed
                                // ((NewsCircularScreen) activity).loadInterstitialAd();
                                // Showing a simple Toast message to user when the user interacted with ad and got the other app and then return to the app again
                                //mView.show(activity.getSupportFragmentManager(), "load");
                                //Toast.makeText(activity, "Interstitial Ad is Closed", Toast.LENGTH_LONG).show();
                                if (mView != null)
                                    mView.dismiss();
                                intent = new Intent(activity, TestScreenSample.class);
                                activity.startActivity(intent);

                                //Toast.makeText(activity, ""+newsList.getNewsCircularId(), Toast.LENGTH_SHORT).show();
                            }
                        });*/




                       /* intent = new Intent(activity, TestScreenSample.class);
                        activity.startActivity(intent);*/
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return listOption.size();
    }

    public int getNumColumns() {
        return this.mNumColumns;
    }

    public void setNumColumns(int numColumns) {
        this.mNumColumns = numColumns;
    }

    @Override
    public void onResult(JSONObject jobjWhole) {
        try {
            if (jobjWhole != null) {
                JSONObject jObj = jobjWhole.optJSONObject(Connection.TAG_DATA);
                int strStatus = jObj.optInt("success");
                String strMessage = jObj.optString("message");
                String strApi = jObj.optString("api");

                if (strStatus != 0) {
                    if (strApi.equalsIgnoreCase("getPdf")) {
                        if (jObj != null) {
                            try {
                                PDF = jObj.optString("pdf");
                                PDFName = jObj.optString("pdf_name");

                                Log.e("pdfresponse", "PDF->" + PDF +
                                        "\nPDFName->" + PDFName);

                                if (!(PDF.isEmpty() && PDFName.isEmpty())) {
                                    intent = new Intent(activity, PdfScreen.class);
                                    intent.putExtra("PDF", PDF);
                                    intent.putExtra("Name", PDFName);
                                    ChapterAdapter.bottomSheetChapterFragment.dismiss();
                                    activity.startActivity(intent);
                                } else {
                                    Toast.makeText(activity, "Data Not Found...!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    Functions.ToastUtility(activity, strMessage);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showInterstitialAd() {
        if (interstitialAd.isLoaded()) {
            //showing the ad Interstitial Ad if it is loaded
            interstitialAd.show();

            // Showing a simple Toast message to user when an Interstitial ad is shown to the user
            //Toast.makeText(getActivity(), "Interstitial is loaded and showing ad  ", Toast.LENGTH_SHORT).show();
        } else {
            //Load the Interstitial ad if it is not loaded
            loadInterstitialAd();

            // Showing a simple Toast message to user when an ad is not loaded
            //Toast.makeText(DaysSpecialScreen.this, "Interstitial Ad is not Loaded ", Toast.LENGTH_SHORT).show();
        }
    }

    public void loadInterstitialAd() {
        // Creating  a Ad Request
        AdRequest adRequest = new AdRequest.Builder().build();
        // load Ad with the Request
        interstitialAd.loadAd(adRequest);
        adRequest.isTestDevice(activity);

        // Showing a simple Toast message to user when an ad is Loading
        //Toast.makeText(DaysSpecialScreen.this, "Interstitial Ad is loading ", Toast.LENGTH_SHORT).show();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final RelativeLayout rlOption;
        private final AppCompatImageView ivOption;
        private final AppCompatTextView tvOption;
        //private AppCompatImageView mIvBoardMemberPic;

        public MyViewHolder(View itemView) {
            super(itemView);
            rlOption = itemView.findViewById(R.id.rl_option);
            ivOption = itemView.findViewById(R.id.iv_option);
            tvOption = itemView.findViewById(R.id.tv_option);
        }
    }
}
