package com.gujeducation.gujaratedu.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.gujeducation.R;
import com.gujeducation.gujaratedu.Adapter.ClassAdapter;
import com.gujeducation.gujaratedu.Helper.Connection;
import com.gujeducation.gujaratedu.Helper.Functions;
import com.gujeducation.gujaratedu.Interface.OnResult;
import com.gujeducation.gujaratedu.Model.Class;
import com.gujeducation.gujaratedu.Model.SubClass;
import com.gujeducation.gujaratedu.ServerAPIs.APIs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AvailableClassScreen extends AppCompatActivity implements OnResult {

    private AppCompatImageView btnBack;
    Intent intent;
    private RecyclerView recyclerClass;
    LinearLayoutManager mLayoutManager;
    private ArrayList<Class> listArrClass = new ArrayList<>();
    private ArrayList<String> listArrCategory = new ArrayList<>();
    Functions mFunctions;
    ClassAdapter mClassAdapter;
    public InterstitialAd interstitialAd;
    AdView mAdView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_class);
        mFunctions = new Functions(this);
        recyclerClass = findViewById(R.id.recyclerview_class);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerClass.setHasFixedSize(true);
        recyclerClass.setLayoutManager(mLayoutManager);
        btnBack = findViewById(R.id.ivback);

        interstitialAd = new InterstitialAd(AvailableClassScreen.this);
        interstitialAd.setAdUnitId("ca-app-pub-6923797063551368/3272831029");

        List<String> testDeviceIds = new ArrayList<String>();
        testDeviceIds.add("8A898BC8824C996E9320D350D4AF1F10");
        testDeviceIds.add("F3CBCC502BD4E653B8B534D769BC587A");
        testDeviceIds.add("FFB848305EE41D5DB1D6C522BFB75BEE");
        testDeviceIds.add("105122E1816DB58B97D2DF2E357E7A37");
        testDeviceIds.add("ED6E76F6E947CC1B01B01524B255999E");

        RequestConfiguration configuration =
                new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
        MobileAds.setRequestConfiguration(configuration);



        if (Functions.knowInternetOn(this)) {
            APIs.getClass(AvailableClassScreen.this,this,mFunctions.getPrefMediumId());
        } else {
            Functions.showInternetAlert(this);
        }
        // set default preference
        mFunctions.setSectionId(0);
        mFunctions.setStandardId(0);
        mFunctions.setStandardIdentify(0);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(AvailableClassScreen.this, HomeScreen.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent = new Intent(AvailableClassScreen.this,HomeScreen.class);
        startActivity(intent);
    }


    @Override
    public void onResult(JSONObject jobjWhole) {

    try {
            if (jobjWhole != null) {
                JSONObject jObj = jobjWhole.optJSONObject(Connection.TAG_DATA);
                int strStatus = jObj.optInt("success");
                String strMessage = jObj.optString("message");

                if (strStatus != 0) {
                    JSONArray jArrayClass = jObj.getJSONArray("class");
                    if (jArrayClass.length() > 0) {
                        listArrClass.clear();
                        for (int i = 0; i < jArrayClass.length(); i++) {
                            try {
                                JSONObject jsonObject = jArrayClass.getJSONObject(i);
                                JSONArray result = jsonObject.getJSONArray("category"+(i+1));
                                if(result.length() != 0) {
                                    Class objClass = new Class(this);
                                    String strCategory = "category" + (i + 1);
                                    if (strCategory.equalsIgnoreCase("category1")) objClass.setCategory(1);
                                    else if (strCategory.equalsIgnoreCase("category2")) objClass.setCategory(2);
                                    else if (strCategory.equalsIgnoreCase("category3")) objClass.setCategory(3);
                                    else if (strCategory.equalsIgnoreCase("category4")) objClass.setCategory(4);
                                    else if (strCategory.equalsIgnoreCase("category5")) objClass.setCategory(5);

                                    ArrayList<SubClass> listSubClass = new ArrayList<>();

                                    for (int j = 0; j < result.length(); j++) {
                                        JSONObject object = result.getJSONObject(j);
                                        //Log.e("Data", "Category" + (i + 1) + "-->" + object.getInt("category"));
                                        SubClass objSubClass = new SubClass();
                                        objSubClass.setClassId(object.optInt("standardId"));
                                        objSubClass.setMediumId(object.optInt("mediumId"));
                                        objSubClass.setIs_identify(object.optInt("is_identify"));
                                        objSubClass.setClassName(object.optString("standard").trim());
                                        listSubClass.add(objSubClass);
                                    }
                                    objClass.setListClass(listSubClass);
                                    listArrClass.add(objClass);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if(listArrClass.size() != 0) {
                            for (int i = 0;i<listArrClass.size();i++){
                                listArrCategory.add(listArrClass.get(i).getCategoryName());
                                listArrCategory = Class.removeDuplicates(listArrCategory);
                            }
                            mClassAdapter= new ClassAdapter(AvailableClassScreen.this, listArrClass, listArrCategory);
                            recyclerClass.setAdapter(mClassAdapter);
                            mClassAdapter.notifyDataSetChanged();
                        }
                        else{
                            Toast.makeText(this, "No Data Found...!", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    mFunctions.ToastUtility(AvailableClassScreen.this, strMessage);
                    //recyclerViewLanguage.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showInterstitialAd() {
        if (interstitialAd.isLoaded()) {
            //showing the ad Interstitial Ad if it is loaded
            interstitialAd.show();

            // Showing a simple Toast message to user when an Interstitial ad is shown to the user
            //Toast.makeText(AvailableClassScreen.this, "Interstitial is loaded and showing ad  ", Toast.LENGTH_SHORT).show();
        } else {
            //Load the Interstitial ad if it is not loaded
            loadInterstitialAd();

            // Showing a simple Toast message to user when an ad is not loaded
            //Toast.makeText(DaysSpecialScreen.this, "Interstitial Ad is not Loaded ", Toast.LENGTH_SHORT).show();
        }
    }

    public void loadInterstitialAd() {
        // Creating  a Ad Request
        AdRequest adRequest = new AdRequest.Builder().build();
        // load Ad with the Request
        interstitialAd.loadAd(adRequest);
        adRequest.isTestDevice(AvailableClassScreen.this);

        // Showing a simple Toast message to user when an ad is Loading
        //Toast.makeText(DaysSpecialScreen.this, "Interstitial Ad is loading ", Toast.LENGTH_SHORT).show();
    }

}
