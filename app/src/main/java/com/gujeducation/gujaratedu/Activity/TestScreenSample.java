package com.gujeducation.gujaratedu.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gujeducation.R;
import com.gujeducation.gujaratedu.Adapter.ChapterAdapter;
import com.gujeducation.gujaratedu.Adapter.TestAdapter;
import com.gujeducation.gujaratedu.Helper.Connection;
import com.gujeducation.gujaratedu.Helper.Functions;
import com.gujeducation.gujaratedu.Interface.OnResult;
import com.gujeducation.gujaratedu.Model.Quiz;
import com.gujeducation.gujaratedu.ServerAPIs.APIs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class TestScreenSample extends AppCompatActivity implements OnResult {
    AppCompatButton btnNextquestion, btnReAttempt;
    AppCompatImageView mIvBackbtn, mIvheader_logo;
    Functions mFunction;
    LinearLayout mLlquestionGrp, mTvNoQuiz, mLlQue1, mLlQue2, mLlQue3, mLlQue4, mLlQue5, mLlQue6, mLlQue7, mLlQue8, mLlQue9, mLlQue10;
    RadioGroup rdQueGrp1, rdQueGrp2, rdQueGrp3, rdQueGrp4, rdQueGrp5, rdQueGrp6, rdQueGrp7, rdQueGrp8, rdQueGrp9, rdQueGrp10;
    String answer1 = "", answer2 = "", answer3 = "", answer4 = "", answer5 = "", answer6 = "", answer7 = "", answer8 = "", answer9 = "", answer10 = "";
    String chapter_Name, subject_Name, standard_NO, correct_answer1 = "",
            correct_answer2 = "",
            correct_answer3 = "",
            correct_answer4 = "", correct_answer5 = "", correct_answer6 = "",
            correct_answer7 = "", correct_answer8 = "", correct_answer9 = "",
            correct_answer10 = "";
    int countAns = 0, outAns = 1, correct_answer_count = 0;
    boolean answer_click = false;

    AppCompatTextView mTvStandard, mTvChapterName, mTvSubject, mTvquestion1, mTvquestion2, mTvquestion3, mTvquestion4, mTvquestion5, mTvquestion6, mTvquestion7, mTvquestion8,
            mTvquestion9, mTvquestion10, mTVQuestionOutOf;
    ArrayList<Quiz> listQuiz = new ArrayList<>();
    //RecyclerView recyclerViewQuiz;
    LinearLayoutManager mLayoutManager;
    TestAdapter mQuizTestAdapter;
    int chapter_ID, semester_ID, subject_ID;
    LinearLayout mLlQuestion1_OptionA,
            mLlQuestion1_OptionB,
            mLlQuestion1_OptionC,
            mLlQuestion1_OptionD,
            mLlQuestion2_OptionA,
            mLlQuestion2_OptionB,
            mLlQuestion2_OptionC,
            mLlQuestion2_OptionD,
            mLlQuestion3_OptionA,
            mLlQuestion3_OptionB,
            mLlQuestion3_OptionC,
            mLlQuestion3_OptionD,
            mLlQuestion4_OptionA,
            mLlQuestion4_OptionB,
            mLlQuestion4_OptionC,
            mLlQuestion4_OptionD,
            mLlQuestion5_OptionA,
            mLlQuestion5_OptionB,
            mLlQuestion5_OptionC,
            mLlQuestion5_OptionD,
            mLlQuestion6_OptionA,
            mLlQuestion6_OptionB,
            mLlQuestion6_OptionC,
            mLlQuestion6_OptionD,
            mLlQuestion7_OptionA,
            mLlQuestion7_OptionB,
            mLlQuestion7_OptionC,
            mLlQuestion7_OptionD,
            mLlQuestion8_OptionA,
            mLlQuestion8_OptionB,
            mLlQuestion8_OptionC,
            mLlQuestion8_OptionD,
            mLlQuestion9_OptionA,
            mLlQuestion9_OptionB,
            mLlQuestion9_OptionC,
            mLlQuestion9_OptionD,
            mLlQuestion10_OptionA,
            mLlQuestion10_OptionB,
            mLlQuestion10_OptionC,
            mLlQuestion10_OptionD;


    AppCompatTextView
            mTvQuestion1_OptionA,
            mTvQuestion1_OptionB,
            mTvQuestion1_OptionC,
            mTvQuestion1_OptionD,
            mTvQuestion2_OptionA,
            mTvQuestion2_OptionB,
            mTvQuestion2_OptionC,
            mTvQuestion2_OptionD,
            mTvQuestion3_OptionA,
            mTvQuestion3_OptionB,
            mTvQuestion3_OptionC,
            mTvQuestion3_OptionD,
            mTvQuestion4_OptionA,
            mTvQuestion4_OptionB,
            mTvQuestion4_OptionC,
            mTvQuestion4_OptionD,
            mTvQuestion5_OptionA,
            mTvQuestion5_OptionB,
            mTvQuestion5_OptionC,
            mTvQuestion5_OptionD,
            mTvQuestion6_OptionA,
            mTvQuestion6_OptionB,
            mTvQuestion6_OptionC,
            mTvQuestion6_OptionD,
            mTvQuestion7_OptionA,
            mTvQuestion7_OptionB,
            mTvQuestion7_OptionC,
            mTvQuestion7_OptionD,
            mTvQuestion8_OptionA,
            mTvQuestion8_OptionB,
            mTvQuestion8_OptionC,
            mTvQuestion8_OptionD,
            mTvQuestion9_OptionA,
            mTvQuestion9_OptionB,
            mTvQuestion9_OptionC,
            mTvQuestion9_OptionD,
            mTvQuestion10_OptionA,
            mTvQuestion10_OptionB,
            mTvQuestion10_OptionC,
            mTvQuestion10_OptionD;

    Animation animShake, animFadein, animSlideinup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // To make activity full screen.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testscreensample);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        ChapterAdapter.bottomSheetChapterFragment.dismiss();
        mFunction = new Functions(this);

        com.gujeducation.gujaratedu.Helper.Language.set(TestScreenSample.this, mFunction.getPrefLanguageCode());
        /*Log.e("GETLANGUAGE_LOGIN", "-" + mFunction.getPrefLanguageCode() +
                " LngCode - " + com.gujeducation.gujaratedu.Helper.Language.getCode());*/
//http://ssamsbsurat.org/android_api/getQuiz.php?mediumId=2&standardId=6&semesterId=3&textbookId=40


        try {
            semester_ID = mFunction.getSemester();
            standard_NO = mFunction.getStandardName();
            subject_ID = mFunction.getSubjectId();
            subject_Name = mFunction.getSubjectName();
            chapter_ID = mFunction.getChapterId();
            chapter_Name = mFunction.getChapterName();

            Log.e("1_TestScrnIntent",
                    "Medium_ID->" + mFunction.getPrefMediumId() +
                            "semester_ID->" + semester_ID + " standard_NO->" + standard_NO +
                            " subject_ID->" + subject_ID + "  subject_Name->" + subject_Name +
                            " chapter_ID->" + chapter_ID + "  chapter_Name->" + chapter_Name);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mTvChapterName = findViewById(R.id.txtchapter);
        mTvChapterName.setText("" + chapter_ID + " - " + mFunction.getChapterName());

        mTvSubject = findViewById(R.id.txtsubject);
        mTvSubject.setText(mFunction.getSubjectName());


        mTvStandard = findViewById(R.id.txtstandard);
        mIvheader_logo = findViewById(R.id.ivheader_logoo);
        mIvBackbtn = findViewById(R.id.ivbackbt);

        //slideinright = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        animShake = AnimationUtils.loadAnimation(this, R.anim.shake_error);
        animFadein = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        // final Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
        animSlideinup = AnimationUtils.loadAnimation(this, R.anim.slide_in_up);
        //final Animation slideoutleft = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        //final Animation slideoutdown = AnimationUtils.loadAnimation(this, R.anim.slide_out_down);
        //final Animation fadeout = AnimationUtils.loadAnimation(this, R.anim.fade_out);


        mLlquestionGrp = findViewById(R.id.llquestiongrp);
        mLlquestionGrp.setVisibility(View.GONE);

        mTVQuestionOutOf = findViewById(R.id.tvquestotl);
        mTVQuestionOutOf.setText("1 / 10");

        mTvNoQuiz = findViewById(R.id.tvnoquiz);
        mTvNoQuiz.setVisibility(View.GONE);

        btnNextquestion = findViewById(R.id.btnNextQue);
        btnNextquestion.setVisibility(View.GONE);
        // btnNextquestion.setText("Next question");
        btnReAttempt = findViewById(R.id.btnReAttempt);
        btnReAttempt.setVisibility(View.GONE);
        //    mIvBack = (AppCompatImageView) findViewById(R.id.ivback);
        mLlQue1 = findViewById(R.id.llquestion1);
        mLlQue2 = findViewById(R.id.llquestion2);
        mLlQue3 = findViewById(R.id.llquestion3);
        mLlQue4 = findViewById(R.id.llquestion4);
        mLlQue5 = findViewById(R.id.llquestion5);
        mLlQue6 = findViewById(R.id.llquestion6);
        mLlQue7 = findViewById(R.id.llquestion7);
        mLlQue8 = findViewById(R.id.llquestion8);
        mLlQue9 = findViewById(R.id.llquestion9);
        mLlQue10 = findViewById(R.id.llquestion10);

        mTvquestion1 = findViewById(R.id.tvquestion1);
        mTvquestion2 = findViewById(R.id.tvquestion2);
        mTvquestion3 = findViewById(R.id.tvquestion3);
        mTvquestion4 = findViewById(R.id.tvquestion4);
        mTvquestion5 = findViewById(R.id.tvquestion5);
        mTvquestion6 = findViewById(R.id.tvquestion6);
        mTvquestion7 = findViewById(R.id.tvquestion7);
        mTvquestion8 = findViewById(R.id.tvquestion8);
        mTvquestion9 = findViewById(R.id.tvquestion9);
        mTvquestion10 = findViewById(R.id.tvquestion10);


        mLlQuestion1_OptionA = findViewById(R.id.lvQue1_OptionA);
        mLlQuestion1_OptionB = findViewById(R.id.lvQue1_OptionB);
        mLlQuestion1_OptionC = findViewById(R.id.lvQue1_OptionC);
        mLlQuestion1_OptionD = findViewById(R.id.lvQue1_OptionD);
        mTvQuestion1_OptionA = findViewById(R.id.tvQue1_OptionA);
        mTvQuestion1_OptionB = findViewById(R.id.tvQue1_OptionB);
        mTvQuestion1_OptionC = findViewById(R.id.tvQue1_OptionC);
        mTvQuestion1_OptionD = findViewById(R.id.tvQue1_OptionD);

        mLlQuestion2_OptionA = findViewById(R.id.lvQue2_OptionA);
        mLlQuestion2_OptionB = findViewById(R.id.lvQue2_OptionB);
        mLlQuestion2_OptionC = findViewById(R.id.lvQue2_OptionC);
        mLlQuestion2_OptionD = findViewById(R.id.lvQue2_OptionD);
        mTvQuestion2_OptionA = findViewById(R.id.tvQue2_OptionA);
        mTvQuestion2_OptionB = findViewById(R.id.tvQue2_OptionB);
        mTvQuestion2_OptionC = findViewById(R.id.tvQue2_OptionC);
        mTvQuestion2_OptionD = findViewById(R.id.tvQue2_OptionD);

        mLlQuestion3_OptionA = findViewById(R.id.lvQue3_OptionA);
        mLlQuestion3_OptionB = findViewById(R.id.lvQue3_OptionB);
        mLlQuestion3_OptionC = findViewById(R.id.lvQue3_OptionC);
        mLlQuestion3_OptionD = findViewById(R.id.lvQue3_OptionD);
        mTvQuestion3_OptionA = findViewById(R.id.tvQue3_OptionA);
        mTvQuestion3_OptionB = findViewById(R.id.tvQue3_OptionB);
        mTvQuestion3_OptionC = findViewById(R.id.tvQue3_OptionC);
        mTvQuestion3_OptionD = findViewById(R.id.tvQue3_OptionD);

        mLlQuestion4_OptionA = findViewById(R.id.lvQue4_OptionA);
        mLlQuestion4_OptionB = findViewById(R.id.lvQue4_OptionB);
        mLlQuestion4_OptionC = findViewById(R.id.lvQue4_OptionC);
        mLlQuestion4_OptionD = findViewById(R.id.lvQue4_OptionD);
        mTvQuestion4_OptionA = findViewById(R.id.tvQue4_OptionA);
        mTvQuestion4_OptionB = findViewById(R.id.tvQue4_OptionB);
        mTvQuestion4_OptionC = findViewById(R.id.tvQue4_OptionC);
        mTvQuestion4_OptionD = findViewById(R.id.tvQue4_OptionD);

        mLlQuestion5_OptionA = findViewById(R.id.lvQue5_OptionA);
        mLlQuestion5_OptionB = findViewById(R.id.lvQue5_OptionB);
        mLlQuestion5_OptionC = findViewById(R.id.lvQue5_OptionC);
        mLlQuestion5_OptionD = findViewById(R.id.lvQue5_OptionD);
        mTvQuestion5_OptionA = findViewById(R.id.tvQue5_OptionA);
        mTvQuestion5_OptionB = findViewById(R.id.tvQue5_OptionB);
        mTvQuestion5_OptionC = findViewById(R.id.tvQue5_OptionC);
        mTvQuestion5_OptionD = findViewById(R.id.tvQue5_OptionD);

        mLlQuestion6_OptionA = findViewById(R.id.lvQue6_OptionA);
        mLlQuestion6_OptionB = findViewById(R.id.lvQue6_OptionB);
        mLlQuestion6_OptionC = findViewById(R.id.lvQue6_OptionC);
        mLlQuestion6_OptionD = findViewById(R.id.lvQue6_OptionD);
        mTvQuestion6_OptionA = findViewById(R.id.tvQue6_OptionA);
        mTvQuestion6_OptionB = findViewById(R.id.tvQue6_OptionB);
        mTvQuestion6_OptionC = findViewById(R.id.tvQue6_OptionC);
        mTvQuestion6_OptionD = findViewById(R.id.tvQue6_OptionD);

        mLlQuestion7_OptionA = findViewById(R.id.lvQue7_OptionA);
        mLlQuestion7_OptionB = findViewById(R.id.lvQue7_OptionB);
        mLlQuestion7_OptionC = findViewById(R.id.lvQue7_OptionC);
        mLlQuestion7_OptionD = findViewById(R.id.lvQue7_OptionD);
        mTvQuestion7_OptionA = findViewById(R.id.tvQue7_OptionA);
        mTvQuestion7_OptionB = findViewById(R.id.tvQue7_OptionB);
        mTvQuestion7_OptionC = findViewById(R.id.tvQue7_OptionC);
        mTvQuestion7_OptionD = findViewById(R.id.tvQue7_OptionD);

        mLlQuestion8_OptionA = findViewById(R.id.lvQue8_OptionA);
        mLlQuestion8_OptionB = findViewById(R.id.lvQue8_OptionB);
        mLlQuestion8_OptionC = findViewById(R.id.lvQue8_OptionC);
        mLlQuestion8_OptionD = findViewById(R.id.lvQue8_OptionD);
        mTvQuestion8_OptionA = findViewById(R.id.tvQue8_OptionA);
        mTvQuestion8_OptionB = findViewById(R.id.tvQue8_OptionB);
        mTvQuestion8_OptionC = findViewById(R.id.tvQue8_OptionC);
        mTvQuestion8_OptionD = findViewById(R.id.tvQue8_OptionD);

        mLlQuestion9_OptionA = findViewById(R.id.lvQue9_OptionA);
        mLlQuestion9_OptionB = findViewById(R.id.lvQue9_OptionB);
        mLlQuestion9_OptionC = findViewById(R.id.lvQue9_OptionC);
        mLlQuestion9_OptionD = findViewById(R.id.lvQue9_OptionD);
        mTvQuestion9_OptionA = findViewById(R.id.tvQue9_OptionA);
        mTvQuestion9_OptionB = findViewById(R.id.tvQue9_OptionB);
        mTvQuestion9_OptionC = findViewById(R.id.tvQue9_OptionC);
        mTvQuestion9_OptionD = findViewById(R.id.tvQue9_OptionD);

        mLlQuestion10_OptionA = findViewById(R.id.lvQue10_OptionA);
        mLlQuestion10_OptionB = findViewById(R.id.lvQue10_OptionB);
        mLlQuestion10_OptionC = findViewById(R.id.lvQue10_OptionC);
        mLlQuestion10_OptionD = findViewById(R.id.lvQue10_OptionD);
        mTvQuestion10_OptionA = findViewById(R.id.tvQue10_OptionA);
        mTvQuestion10_OptionB = findViewById(R.id.tvQue10_OptionB);
        mTvQuestion10_OptionC = findViewById(R.id.tvQue10_OptionC);
        mTvQuestion10_OptionD = findViewById(R.id.tvQue10_OptionD);


        mLlQuestion1_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answer1 = mTvQuestion1_OptionA.getText().toString().trim();
                if (answer1.trim().equalsIgnoreCase(correct_answer1.trim())) {
                    mTvQuestion1_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion1_OptionA.startAnimation(animFadein);
                    mTvQuestion1_OptionA.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion1_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion1_OptionA.startAnimation(animShake);
                    mTvQuestion1_OptionA.setTextColor(getResources().getColor(R.color.white));
                    if (correct_answer1.equalsIgnoreCase(mTvQuestion1_OptionB.getText().toString().trim())) {
                        mTvQuestion1_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionB.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionB.startAnimation(animFadein);
                    } else if (correct_answer1.equalsIgnoreCase(mTvQuestion1_OptionC.getText().toString().trim())) {
                        mTvQuestion1_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionC.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionC.startAnimation(animFadein);
                    } else {
                        mTvQuestion1_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion1_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answer1 = mTvQuestion1_OptionB.getText().toString().trim();
                if (answer1.trim().equalsIgnoreCase(correct_answer1.trim())) {
                    mTvQuestion1_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion1_OptionB.startAnimation(animFadein);
                    mTvQuestion1_OptionB.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    Log.e("mLlQuestion1_OptionB", "" + answer_click);
                    answer_click = true;
                    mTvQuestion1_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion1_OptionB.startAnimation(animShake);
                    if (correct_answer1.equalsIgnoreCase(mTvQuestion1_OptionA.getText().toString().trim())) {
                        mTvQuestion1_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion1_OptionA.startAnimation(animFadein);
                        mTvQuestion1_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer1.equalsIgnoreCase(mTvQuestion1_OptionC.getText().toString().trim())) {
                        mTvQuestion1_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion1_OptionC.startAnimation(animFadein);
                        mTvQuestion1_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion1_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionD.startAnimation(animFadein);
                    }

                }
            }
        });
        mLlQuestion1_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer1 = mTvQuestion1_OptionC.getText().toString().trim();
                if (answer1.trim().equalsIgnoreCase(correct_answer1.trim())) {
                    mTvQuestion1_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion1_OptionC.startAnimation(animFadein);
                    mTvQuestion1_OptionC.setTextColor(getResources().getColor(R.color.white));

                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion1_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion1_OptionC.startAnimation(animShake);

                    if (correct_answer1.equalsIgnoreCase(mTvQuestion1_OptionA.getText().toString().trim())) {
                        mTvQuestion1_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionA.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionA.startAnimation(animFadein);
                    } else if (correct_answer1.equalsIgnoreCase(mTvQuestion1_OptionB.getText().toString().trim())) {
                        mTvQuestion1_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionB.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionB.startAnimation(animFadein);
                    } else {
                        mTvQuestion1_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion1_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answer1 = mTvQuestion1_OptionD.getText().toString().trim();
                if (answer1.trim().equalsIgnoreCase(correct_answer1.trim())) {
                    mTvQuestion1_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion1_OptionD.startAnimation(animFadein);
                    mTvQuestion1_OptionD.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion1_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion1_OptionD.startAnimation(animShake);
                    if (correct_answer1.equalsIgnoreCase(mTvQuestion1_OptionA.getText().toString().trim())) {
                        mTvQuestion1_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionA.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionA.startAnimation(animFadein);
                    } else if (correct_answer1.equalsIgnoreCase(mTvQuestion1_OptionB.getText().toString().trim())) {
                        mTvQuestion1_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionB.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionB.startAnimation(animFadein);
                    } else {
                        mTvQuestion1_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion1_OptionC.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion1_OptionC.startAnimation(animFadein);

                    }
                }
            }
        });

        mLlQuestion2_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answer2 = mTvQuestion2_OptionA.getText().toString().trim();
                if (answer2.trim().equalsIgnoreCase(correct_answer2.trim())) {
                    mTvQuestion2_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mTvQuestion2_OptionA.setTextColor(getResources().getColor(R.color.white));
                    mLlQuestion2_OptionA.startAnimation(animFadein);
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion2_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion2_OptionA.startAnimation(animShake);
                    if (correct_answer2.equalsIgnoreCase(mTvQuestion2_OptionB.getText().toString().trim())) {
                        mTvQuestion2_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion2_OptionB.startAnimation(animFadein);
                        mTvQuestion2_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer2.equalsIgnoreCase(mTvQuestion2_OptionC.getText().toString().trim())) {
                        mTvQuestion2_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion2_OptionC.startAnimation(animFadein);
                        mTvQuestion2_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion2_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion2_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion2_OptionD.startAnimation(animFadein);
                    }

                }
            }
        });
        mLlQuestion2_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer2 = mTvQuestion2_OptionB.getText().toString().trim();
                if (answer2.trim().equalsIgnoreCase(correct_answer2.trim())) {
                    mTvQuestion2_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion2_OptionB.startAnimation(animFadein);
                    mTvQuestion2_OptionB.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion2_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion2_OptionB.startAnimation(animShake);
                    if (correct_answer2.equalsIgnoreCase(mTvQuestion2_OptionA.getText().toString().trim())) {
                        mTvQuestion2_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion2_OptionA.startAnimation(animFadein);
                        mTvQuestion2_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer2.equalsIgnoreCase(mTvQuestion2_OptionC.getText().toString().trim())) {
                        mTvQuestion2_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion2_OptionC.startAnimation(animFadein);
                        mTvQuestion2_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion2_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion2_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion2_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion2_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer2 = mTvQuestion2_OptionC.getText().toString().trim();
                if (answer2.trim().equalsIgnoreCase(correct_answer2.trim())) {
                    mTvQuestion2_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion2_OptionC.startAnimation(animFadein);
                    mTvQuestion2_OptionC.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion2_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion2_OptionC.startAnimation(animShake);
                    if (correct_answer2.equalsIgnoreCase(mTvQuestion2_OptionA.getText().toString().trim())) {
                        mTvQuestion2_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion2_OptionA.startAnimation(animFadein);
                        mTvQuestion2_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer2.equalsIgnoreCase(mTvQuestion2_OptionB.getText().toString().trim())) {
                        mTvQuestion2_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion2_OptionB.startAnimation(animFadein);
                        mTvQuestion2_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion2_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion2_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion2_OptionD.startAnimation(animFadein);
                    }

                }
            }
        });
        mLlQuestion2_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer2 = mTvQuestion2_OptionD.getText().toString().trim();
                if (answer2.trim().equalsIgnoreCase(correct_answer2.trim())) {
                    mTvQuestion2_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion2_OptionD.startAnimation(animFadein);
                    mTvQuestion2_OptionD.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion2_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion2_OptionD.startAnimation(animShake);
                    if (correct_answer2.equalsIgnoreCase(mTvQuestion2_OptionA.getText().toString().trim())) {
                        mTvQuestion2_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion2_OptionA.startAnimation(animFadein);
                        mTvQuestion2_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer2.equalsIgnoreCase(mTvQuestion2_OptionB.getText().toString().trim())) {
                        mTvQuestion2_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion2_OptionB.startAnimation(animFadein);
                        mTvQuestion2_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion2_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion2_OptionC.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion2_OptionC.startAnimation(animFadein);
                    }
                }
            }
        });

        mLlQuestion3_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer3 = mTvQuestion3_OptionA.getText().toString().trim();
                if (answer3.trim().equalsIgnoreCase(correct_answer3.trim())) {
                    mTvQuestion3_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion3_OptionA.startAnimation(animFadein);
                    mTvQuestion3_OptionA.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion3_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mTvQuestion3_OptionA.startAnimation(animShake);
                    if (correct_answer3.equalsIgnoreCase(mTvQuestion3_OptionB.getText().toString().trim())) {
                        mTvQuestion3_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion3_OptionB.startAnimation(animFadein);
                        mTvQuestion3_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer3.equalsIgnoreCase(mTvQuestion3_OptionC.getText().toString().trim())) {
                        mTvQuestion3_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion3_OptionC.startAnimation(animFadein);
                        mTvQuestion3_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion3_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion3_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion3_OptionD.startAnimation(animFadein);
                    }

                }
            }
        });
        mLlQuestion3_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer3 = mTvQuestion3_OptionB.getText().toString().trim();
                if (answer3.trim().equalsIgnoreCase(correct_answer3.trim())) {
                    mTvQuestion3_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion3_OptionB.startAnimation(animFadein);
                    mTvQuestion3_OptionB.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion3_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion3_OptionB.startAnimation(animShake);
                    if (correct_answer3.equalsIgnoreCase(mTvQuestion3_OptionA.getText().toString().trim())) {
                        mTvQuestion3_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion3_OptionA.startAnimation(animFadein);
                        mTvQuestion3_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer3.equalsIgnoreCase(mTvQuestion3_OptionC.getText().toString().trim())) {
                        mTvQuestion3_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion3_OptionC.startAnimation(animFadein);
                        mTvQuestion3_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion3_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion3_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion3_OptionD.startAnimation(animFadein);
                    }

                }
            }
        });
        mLlQuestion3_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer3 = mTvQuestion3_OptionC.getText().toString().trim();
                if (answer3.trim().equalsIgnoreCase(correct_answer3.trim())) {
                    mTvQuestion3_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion3_OptionC.startAnimation(animFadein);
                    mTvQuestion3_OptionC.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion3_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion3_OptionC.startAnimation(animShake);
                    if (correct_answer3.equalsIgnoreCase(mTvQuestion3_OptionA.getText().toString().trim())) {
                        mTvQuestion3_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion3_OptionA.startAnimation(animFadein);
                        mTvQuestion3_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer3.equalsIgnoreCase(mTvQuestion3_OptionB.getText().toString().trim())) {
                        mTvQuestion3_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion3_OptionB.startAnimation(animFadein);
                        mTvQuestion3_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion3_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion3_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion3_OptionD.startAnimation(animFadein);
                    }

                }
            }
        });
        mLlQuestion3_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer3 = mTvQuestion3_OptionD.getText().toString().trim();
                if (answer3.trim().equalsIgnoreCase(correct_answer3.trim())) {
                    mTvQuestion3_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion3_OptionD.startAnimation(animFadein);
                    mTvQuestion3_OptionD.setTextColor(getResources().getColor(R.color.white));

                    countAns++;
                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion3_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion3_OptionD.startAnimation(animShake);
                    if (correct_answer3.equalsIgnoreCase(mTvQuestion3_OptionA.getText().toString().trim())) {
                        mTvQuestion3_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion3_OptionA.startAnimation(animFadein);
                        mTvQuestion3_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer3.equalsIgnoreCase(mTvQuestion3_OptionB.getText().toString().trim())) {
                        mTvQuestion3_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion3_OptionB.startAnimation(animFadein);
                        mTvQuestion3_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion3_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion3_OptionC.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion3_OptionC.startAnimation(animFadein);
                    }
                }
            }
        });

        mLlQuestion4_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer4 = mTvQuestion4_OptionA.getText().toString().trim();
                if (answer4.trim().equalsIgnoreCase(correct_answer4.trim())) {
                    mTvQuestion4_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion4_OptionA.startAnimation(animFadein);
                    mTvQuestion4_OptionA.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion4_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion4_OptionA.startAnimation(animShake);
                    if (correct_answer4.equalsIgnoreCase(mTvQuestion4_OptionB.getText().toString().trim())) {
                        mTvQuestion4_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion4_OptionB.startAnimation(animFadein);
                        mTvQuestion4_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer4.equalsIgnoreCase(mTvQuestion4_OptionC.getText().toString().trim())) {
                        mTvQuestion4_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion4_OptionC.startAnimation(animFadein);
                        mTvQuestion4_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion4_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion4_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion4_OptionD.startAnimation(animFadein);
                    }


                }
            }
        });
        mLlQuestion4_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer4 = mTvQuestion4_OptionB.getText().toString().trim();
                if (answer4.trim().equalsIgnoreCase(correct_answer4.trim())) {
                    mTvQuestion4_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion4_OptionB.startAnimation(animFadein);
                    countAns++;

                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion4_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion4_OptionB.startAnimation(animShake);
                    if (correct_answer4.equalsIgnoreCase(mTvQuestion4_OptionA.getText().toString().trim())) {
                        mTvQuestion4_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion4_OptionA.startAnimation(animFadein);
                        mTvQuestion4_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer4.equalsIgnoreCase(mTvQuestion4_OptionC.getText().toString().trim())) {
                        mTvQuestion4_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion4_OptionC.startAnimation(animFadein);
                        mTvQuestion4_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion4_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion4_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion4_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion4_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer4 = mTvQuestion4_OptionC.getText().toString().trim();
                if (answer4.trim().equalsIgnoreCase(correct_answer4.trim())) {
                    mTvQuestion4_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion4_OptionC.startAnimation(animFadein);
                    mTvQuestion4_OptionC.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion4_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion4_OptionC.startAnimation(animShake);
                    if (correct_answer4.equalsIgnoreCase(mTvQuestion4_OptionA.getText().toString().trim())) {
                        mTvQuestion4_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion4_OptionA.startAnimation(animFadein);
                        mTvQuestion4_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer4.equalsIgnoreCase(mTvQuestion4_OptionB.getText().toString().trim())) {
                        mTvQuestion4_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion4_OptionB.startAnimation(animFadein);
                        mTvQuestion4_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion4_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion4_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion4_OptionD.startAnimation(animFadein);
                    }


                }
            }
        });
        mLlQuestion4_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer4 = mTvQuestion4_OptionD.getText().toString().trim();
                if (answer4.trim().equalsIgnoreCase(correct_answer4.trim())) {
                    mTvQuestion4_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion4_OptionD.startAnimation(animFadein);
                    mTvQuestion4_OptionD.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion4_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion4_OptionD.startAnimation(animShake);
                    if (correct_answer4.equalsIgnoreCase(mTvQuestion4_OptionA.getText().toString().trim())) {
                        mTvQuestion4_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion4_OptionA.startAnimation(animFadein);
                        mTvQuestion4_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer4.equalsIgnoreCase(mTvQuestion4_OptionB.getText().toString().trim())) {
                        mTvQuestion4_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion4_OptionB.startAnimation(animFadein);
                        mTvQuestion4_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion4_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion4_OptionC.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion4_OptionC.startAnimation(animFadein);
                    }

                }
            }
        });

        mLlQuestion5_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer5 = mTvQuestion5_OptionA.getText().toString().trim();
                if (answer5.trim().equalsIgnoreCase(correct_answer5.trim())) {
                    mTvQuestion5_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion5_OptionA.startAnimation(animFadein);
                    mTvQuestion5_OptionA.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion5_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion5_OptionA.startAnimation(animShake);
                    if (correct_answer5.equalsIgnoreCase(mTvQuestion5_OptionB.getText().toString().trim())) {
                        mTvQuestion5_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion5_OptionB.startAnimation(animFadein);
                        mTvQuestion5_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer5.equalsIgnoreCase(mTvQuestion5_OptionC.getText().toString().trim())) {
                        mTvQuestion5_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion5_OptionC.startAnimation(animFadein);
                        mTvQuestion5_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion5_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion5_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion5_OptionD.startAnimation(animFadein);
                    }


                }
            }
        });
        mLlQuestion5_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer5 = mTvQuestion5_OptionB.getText().toString().trim();
                if (answer5.trim().equalsIgnoreCase(correct_answer5.trim())) {
                    mTvQuestion5_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion5_OptionB.startAnimation(animFadein);
                    mTvQuestion5_OptionB.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion5_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion5_OptionB.startAnimation(animShake);
                    if (correct_answer5.equalsIgnoreCase(mTvQuestion5_OptionA.getText().toString().trim())) {
                        mTvQuestion5_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion5_OptionA.startAnimation(animFadein);
                        mTvQuestion5_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer5.equalsIgnoreCase(mTvQuestion5_OptionC.getText().toString().trim())) {
                        mTvQuestion5_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion5_OptionC.startAnimation(animFadein);
                        mTvQuestion5_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion5_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion5_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion5_OptionD.startAnimation(animFadein);
                    }


                }
            }
        });
        mLlQuestion5_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer5 = mTvQuestion5_OptionC.getText().toString().trim();
                if (answer5.trim().equalsIgnoreCase(correct_answer5.trim())) {
                    mTvQuestion5_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion5_OptionC.startAnimation(animFadein);
                    mTvQuestion5_OptionC.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion5_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion5_OptionC.startAnimation(animShake);
                    if (correct_answer5.equalsIgnoreCase(mTvQuestion5_OptionA.getText().toString().trim())) {
                        mTvQuestion5_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion5_OptionA.startAnimation(animFadein);
                        mTvQuestion5_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer5.equalsIgnoreCase(mTvQuestion5_OptionB.getText().toString().trim())) {
                        mTvQuestion5_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion5_OptionB.startAnimation(animFadein);
                        mTvQuestion5_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion5_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion5_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion5_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion5_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer5 = mTvQuestion5_OptionD.getText().toString().trim();
                if (answer5.trim().equalsIgnoreCase(correct_answer5.trim())) {
                    mTvQuestion5_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion5_OptionD.startAnimation(animFadein);
                    mTvQuestion5_OptionD.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion5_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion5_OptionD.startAnimation(animShake);
                    if (correct_answer5.equalsIgnoreCase(mTvQuestion5_OptionA.getText().toString().trim())) {
                        mTvQuestion5_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion5_OptionA.startAnimation(animFadein);
                        mTvQuestion5_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer5.equalsIgnoreCase(mTvQuestion5_OptionB.getText().toString().trim())) {
                        mTvQuestion5_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion5_OptionB.startAnimation(animFadein);
                        mTvQuestion5_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion5_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion5_OptionC.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion5_OptionC.startAnimation(animFadein);
                    }
                }
            }
        });

        mLlQuestion6_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer6 = mTvQuestion6_OptionA.getText().toString().trim();
                if (answer6.trim().equalsIgnoreCase(correct_answer6.trim())) {
                    mTvQuestion6_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion6_OptionA.startAnimation(animFadein);
                    mTvQuestion6_OptionA.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion6_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion6_OptionA.startAnimation(animShake);
                    if (correct_answer6.equalsIgnoreCase(mTvQuestion6_OptionB.getText().toString().trim())) {
                        mTvQuestion6_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion6_OptionB.startAnimation(animFadein);
                        mTvQuestion6_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer6.equalsIgnoreCase(mTvQuestion6_OptionC.getText().toString().trim())) {
                        mTvQuestion6_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion6_OptionC.startAnimation(animFadein);
                        mTvQuestion6_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion6_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion6_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion6_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion6_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer6 = mTvQuestion6_OptionB.getText().toString().trim();
                if (answer6.trim().equalsIgnoreCase(correct_answer6.trim())) {
                    mTvQuestion6_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion6_OptionB.startAnimation(animFadein);
                    mTvQuestion6_OptionB.setTextColor(getResources().getColor(R.color.white));
                    countAns++;

                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion6_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion6_OptionB.startAnimation(animShake);
                    if (correct_answer6.equalsIgnoreCase(mTvQuestion6_OptionA.getText().toString().trim())) {
                        mTvQuestion6_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion6_OptionA.startAnimation(animFadein);
                        mTvQuestion6_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer6.equalsIgnoreCase(mTvQuestion6_OptionC.getText().toString().trim())) {
                        mTvQuestion6_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion6_OptionC.startAnimation(animFadein);
                        mTvQuestion6_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion6_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion6_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion6_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion6_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answer6 = mTvQuestion6_OptionC.getText().toString().trim();
                if (answer6.trim().equalsIgnoreCase(correct_answer6.trim())) {
                    mTvQuestion6_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion6_OptionC.startAnimation(animFadein);
                    mTvQuestion6_OptionC.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion6_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion6_OptionC.startAnimation(animShake);
                    if (correct_answer6.equalsIgnoreCase(mTvQuestion6_OptionA.getText().toString().trim())) {
                        mTvQuestion6_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion6_OptionA.startAnimation(animFadein);
                        mTvQuestion6_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer6.equalsIgnoreCase(mTvQuestion6_OptionB.getText().toString().trim())) {
                        mTvQuestion6_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion6_OptionB.startAnimation(animFadein);
                        mTvQuestion6_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion6_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion6_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion6_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion6_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer6 = mTvQuestion6_OptionD.getText().toString().trim();
                if (answer6.trim().equalsIgnoreCase(correct_answer6.trim())) {
                    mTvQuestion6_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion6_OptionD.startAnimation(animFadein);
                    mTvQuestion6_OptionD.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion6_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion6_OptionD.startAnimation(animShake);
                    if (correct_answer6.equalsIgnoreCase(mTvQuestion6_OptionA.getText().toString().trim())) {
                        mTvQuestion6_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion6_OptionA.startAnimation(animFadein);
                        mTvQuestion6_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer6.equalsIgnoreCase(mTvQuestion6_OptionB.getText().toString().trim())) {
                        mTvQuestion6_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion6_OptionB.startAnimation(animFadein);
                        mTvQuestion6_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion6_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion6_OptionC.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion6_OptionC.startAnimation(animFadein);
                    }
                }
            }
        });

        mLlQuestion7_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer7 = mTvQuestion7_OptionA.getText().toString().trim();
                if (answer7.trim().equalsIgnoreCase(correct_answer7.trim())) {
                    mTvQuestion7_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion7_OptionA.startAnimation(animFadein);
                    mTvQuestion7_OptionA.setTextColor(getResources().getColor(R.color.white));

                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion7_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion7_OptionA.startAnimation(animShake);
                    if (correct_answer7.equalsIgnoreCase(mTvQuestion7_OptionB.getText().toString().trim())) {
                        mTvQuestion7_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion7_OptionB.startAnimation(animFadein);
                        mTvQuestion7_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer7.equalsIgnoreCase(mTvQuestion7_OptionC.getText().toString().trim())) {
                        mTvQuestion7_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion7_OptionC.startAnimation(animFadein);
                        mTvQuestion7_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion7_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion7_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion7_OptionD.startAnimation(animFadein);
                    }


                }
            }
        });
        mLlQuestion7_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer7 = mTvQuestion7_OptionB.getText().toString().trim();
                if (answer7.trim().equalsIgnoreCase(correct_answer7.trim())) {
                    mTvQuestion7_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion7_OptionB.startAnimation(animFadein);
                    mTvQuestion7_OptionB.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion7_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion7_OptionB.startAnimation(animShake);
                    if (correct_answer7.equalsIgnoreCase(mTvQuestion7_OptionA.getText().toString().trim())) {
                        mTvQuestion7_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion7_OptionA.startAnimation(animFadein);
                        mTvQuestion7_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer7.equalsIgnoreCase(mTvQuestion7_OptionC.getText().toString().trim())) {
                        mTvQuestion7_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion7_OptionC.startAnimation(animFadein);
                        mTvQuestion7_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion7_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion7_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion7_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion7_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer7 = mTvQuestion7_OptionC.getText().toString().trim();
                if (answer7.trim().equalsIgnoreCase(correct_answer7.trim())) {
                    mTvQuestion7_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion7_OptionC.startAnimation(animFadein);
                    mTvQuestion7_OptionC.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion7_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion7_OptionC.startAnimation(animShake);
                    if (correct_answer7.equalsIgnoreCase(mTvQuestion7_OptionA.getText().toString().trim())) {
                        mTvQuestion7_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion7_OptionA.startAnimation(animFadein);
                        mTvQuestion7_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer7.equalsIgnoreCase(mTvQuestion7_OptionB.getText().toString().trim())) {
                        mTvQuestion7_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion7_OptionB.startAnimation(animFadein);
                        mTvQuestion7_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion7_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion7_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion7_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion7_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer7 = mTvQuestion7_OptionD.getText().toString().trim();
                if (answer7.trim().equalsIgnoreCase(correct_answer7.trim())) {
                    mTvQuestion7_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion7_OptionD.startAnimation(animFadein);
                    mTvQuestion7_OptionD.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion7_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion7_OptionD.startAnimation(animShake);
                    if (correct_answer7.equalsIgnoreCase(mTvQuestion7_OptionA.getText().toString().trim())) {
                        mTvQuestion7_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion7_OptionA.startAnimation(animFadein);
                        mTvQuestion7_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer7.equalsIgnoreCase(mTvQuestion7_OptionB.getText().toString().trim())) {
                        mTvQuestion7_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion7_OptionB.startAnimation(animFadein);
                        mTvQuestion7_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion7_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion7_OptionC.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion7_OptionC.startAnimation(animFadein);
                    }
                }
            }
        });

        mLlQuestion8_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer8 = mTvQuestion8_OptionA.getText().toString().trim();
                if (answer8.trim().equalsIgnoreCase(correct_answer8.trim())) {
                    mTvQuestion8_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion8_OptionA.startAnimation(animFadein);
                    mTvQuestion8_OptionA.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion8_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion8_OptionA.startAnimation(animShake);
                    if (correct_answer8.equalsIgnoreCase(mTvQuestion8_OptionB.getText().toString().trim())) {
                        mTvQuestion8_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion8_OptionB.startAnimation(animFadein);
                        mTvQuestion8_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer8.equalsIgnoreCase(mTvQuestion8_OptionC.getText().toString().trim())) {
                        mTvQuestion8_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion8_OptionC.startAnimation(animFadein);
                        mTvQuestion8_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion8_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion8_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion8_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion8_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer8 = mTvQuestion8_OptionB.getText().toString().trim();
                if (answer8.trim().equalsIgnoreCase(correct_answer8.trim())) {
                    mTvQuestion8_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion8_OptionB.startAnimation(animFadein);
                    mTvQuestion8_OptionB.setTextColor(getResources().getColor(R.color.white));
                    countAns++;

                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion8_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion8_OptionB.startAnimation(animShake);

                    if (correct_answer8.equalsIgnoreCase(mTvQuestion8_OptionA.getText().toString().trim())) {
                        mTvQuestion8_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion8_OptionA.startAnimation(animFadein);
                        mTvQuestion8_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer8.equalsIgnoreCase(mTvQuestion8_OptionC.getText().toString().trim())) {
                        mTvQuestion8_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion8_OptionC.startAnimation(animFadein);
                        mTvQuestion8_OptionC.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion8_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion8_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion8_OptionD.startAnimation(animFadein);
                    }

                }
            }
        });
        mLlQuestion8_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer8 = mTvQuestion8_OptionC.getText().toString().trim();
                if (answer8.trim().equalsIgnoreCase(correct_answer8.trim())) {
                    mTvQuestion8_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion8_OptionC.startAnimation(animFadein);
                    mTvQuestion8_OptionC.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                } else {
                    answer_click = true;
                    mTvQuestion8_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion8_OptionC.startAnimation(animShake);
                    if (correct_answer8.equalsIgnoreCase(mTvQuestion8_OptionA.getText().toString().trim())) {
                        mTvQuestion8_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion8_OptionA.startAnimation(animFadein);
                        mTvQuestion8_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer8.equalsIgnoreCase(mTvQuestion8_OptionB.getText().toString().trim())) {
                        mTvQuestion8_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion8_OptionB.startAnimation(animFadein);
                        mTvQuestion8_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion8_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion8_OptionD.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion8_OptionD.startAnimation(animFadein);
                    }
                }
            }
        });
        mLlQuestion8_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer8 = mTvQuestion8_OptionD.getText().toString().trim();
                if (answer8.trim().equalsIgnoreCase(correct_answer8.trim())) {
                    mTvQuestion8_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion8_OptionD.startAnimation(animFadein);
                    mTvQuestion8_OptionD.setTextColor(getResources().getColor(R.color.white));
                    countAns++;
                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    answer_click = true;
                    mTvQuestion8_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion8_OptionD.startAnimation(animShake);
                    if (correct_answer8.equalsIgnoreCase(mTvQuestion8_OptionA.getText().toString().trim())) {
                        mTvQuestion8_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion8_OptionA.startAnimation(animFadein);
                        mTvQuestion8_OptionA.setTextColor(getResources().getColor(R.color.white));
                    } else if (correct_answer8.equalsIgnoreCase(mTvQuestion8_OptionB.getText().toString().trim())) {
                        mTvQuestion8_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mLlQuestion8_OptionB.startAnimation(animFadein);
                        mTvQuestion8_OptionB.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        mTvQuestion8_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                        mTvQuestion8_OptionC.setTextColor(getResources().getColor(R.color.white));
                        mLlQuestion8_OptionC.startAnimation(animFadein);
                    }

                }
            }
        });

        mLlQuestion9_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer9 = mTvQuestion9_OptionA.getText().toString().trim();
                if (answer9.trim().equalsIgnoreCase(correct_answer9.trim())) {
                    mTvQuestion9_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion9_OptionA.startAnimation(animFadein);
                    countAns++;
                } else {
                    mTvQuestion9_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion9_OptionA.startAnimation(animShake);
                }
            }
        });
        mLlQuestion9_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer9 = mTvQuestion9_OptionB.getText().toString().trim();
                if (answer9.trim().equalsIgnoreCase(correct_answer9.trim())) {
                    mTvQuestion9_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion9_OptionB.startAnimation(animFadein);
                    countAns++;

                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    mTvQuestion9_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion9_OptionB.startAnimation(animShake);

                }
            }
        });
        mLlQuestion9_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer9 = mTvQuestion9_OptionC.getText().toString().trim();
                if (answer9.trim().equalsIgnoreCase(correct_answer9.trim())) {
                    mTvQuestion9_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion9_OptionC.startAnimation(animFadein);
                    countAns++;
                } else {
                    mTvQuestion9_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion9_OptionC.startAnimation(animShake);

                }
            }
        });
        mLlQuestion9_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                answer9 = mTvQuestion9_OptionD.getText().toString().trim();
                if (answer9.trim().equalsIgnoreCase(correct_answer9.trim())) {
                    mTvQuestion9_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion9_OptionD.startAnimation(animFadein);
                    countAns++;
                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    mTvQuestion9_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion9_OptionD.startAnimation(animShake);

                }
            }
        });

        mLlQuestion10_OptionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                answer10 = mTvQuestion10_OptionA.getText().toString().trim();
                if (answer10.trim().equalsIgnoreCase(correct_answer10.trim())) {
                    mTvQuestion10_OptionA.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion10_OptionA.startAnimation(animFadein);
                    countAns++;

                } else {
                    mTvQuestion10_OptionA.setBackgroundResource(R.drawable.shape_mcq_incorrect);

                    mLlQuestion10_OptionA.startAnimation(animShake);
                }
            }
        });
        mLlQuestion10_OptionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                answer10 = mTvQuestion10_OptionB.getText().toString().trim();
                if (answer10.trim().equalsIgnoreCase(correct_answer10.trim())) {
                    mTvQuestion10_OptionB.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion10_OptionB.startAnimation(animFadein);
                    countAns++;


                    //   Log.e("count_2 ", "-> " + countAns++);
                } else {
                    mTvQuestion10_OptionB.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion10_OptionB.startAnimation(animShake);


                }
            }
        });
        mLlQuestion10_OptionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                answer10 = mTvQuestion10_OptionC.getText().toString().trim();
                if (answer10.trim().equalsIgnoreCase(correct_answer10.trim())) {
                    mTvQuestion10_OptionC.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion10_OptionC.startAnimation(animFadein);
                    countAns++;

                } else {
                    mTvQuestion10_OptionC.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion10_OptionC.startAnimation(animShake);


                }
            }
        });
        mLlQuestion10_OptionD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                answer10 = mTvQuestion10_OptionD.getText().toString().trim();
                if (answer10.trim().equalsIgnoreCase(correct_answer10.trim())) {
                    mTvQuestion10_OptionD.setBackgroundResource(R.drawable.shape_mcq_correct);
                    mLlQuestion10_OptionD.startAnimation(animFadein);
                    countAns++;
                } else {
                    mTvQuestion10_OptionD.setBackgroundResource(R.drawable.shape_mcq_incorrect);
                    mLlQuestion10_OptionD.startAnimation(animShake);


                }
            }
        });


        btnNextquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                outAns = countAns + 1;
                /*Log.e("btnNextquestion ", "countAns-> " + countAns +
                        "\n outAns-> " + outAns);*/
                if (outAns != 11) {
                    mTVQuestionOutOf.setText(outAns + " / 10");
                } else {
                    mTVQuestionOutOf.setText("10 / 10");
                }
                Log.e("btnNextquestion-", "countAns-->" + countAns + " answer_click-->" + answer_click);


                if (mLlQue1.getVisibility() == View.VISIBLE) {
                    mLlQue1.setVisibility(View.GONE);
                    mLlQue2.setVisibility(View.VISIBLE);
                    mLlQue2.startAnimation(animSlideinup);
                } else if (mLlQue2.getVisibility() == View.VISIBLE) {
                    mLlQue2.setVisibility(View.GONE);
                    mLlQue3.setVisibility(View.VISIBLE);
                    mLlQue3.startAnimation(animSlideinup);
                } else if (mLlQue3.getVisibility() == View.VISIBLE) {
                    mLlQue3.setVisibility(View.GONE);
                    mLlQue4.setVisibility(View.VISIBLE);
                    mLlQue4.startAnimation(animSlideinup);
                } else if (mLlQue4.getVisibility() == View.VISIBLE) {
                    mLlQue4.setVisibility(View.GONE);
                    mLlQue5.setVisibility(View.VISIBLE);
                    mLlQue5.startAnimation(animSlideinup);
                } else if (mLlQue5.getVisibility() == View.VISIBLE) {
                    mLlQue5.setVisibility(View.GONE);
                    mLlQue6.setVisibility(View.VISIBLE);
                    mLlQue6.startAnimation(animSlideinup);
                } else if (mLlQue6.getVisibility() == View.VISIBLE) {
                    mLlQue6.setVisibility(View.GONE);
                    mLlQue7.setVisibility(View.VISIBLE);
                    mLlQue7.startAnimation(animSlideinup);
                } else if (mLlQue7.getVisibility() == View.VISIBLE) {
                    mLlQue7.setVisibility(View.GONE);
                    mLlQue8.setVisibility(View.VISIBLE);
                    mLlQue8.startAnimation(animSlideinup);
                } else if (mLlQue8.getVisibility() == View.VISIBLE) {
                    mLlQue8.setVisibility(View.GONE);
                    mLlQue9.setVisibility(View.VISIBLE);
                    mLlQue9.startAnimation(animSlideinup);
                } else if (mLlQue9.getVisibility() == View.VISIBLE) {
                    mLlQue9.setVisibility(View.GONE);
                    mLlQue10.setVisibility(View.VISIBLE);
                    mLlQue10.startAnimation(animSlideinup);
                } else if (mLlQue10.getVisibility() == View.VISIBLE) {
                    Log.e("test", "completed-" + countAns);
                    Functions.ToastUtility(TestScreenSample.this, "Thank You");
                    btnNextquestion.setVisibility(View.GONE);
                    btnReAttempt.setVisibility(View.VISIBLE);
                }

                /*if (countAns == 1) {// ||
                    //mTVQuestionOutOf.setText("1 / 10");
                    mLlQue1.setVisibility(View.GONE);
                    mLlQue2.setVisibility(View.VISIBLE);
                    mLlQue2.startAnimation(animSlideinup);
                } else if (countAns == 2) {// ||
                    mLlQue2.setVisibility(View.GONE);
                    mLlQue3.setVisibility(View.VISIBLE);
                    mLlQue3.startAnimation(animSlideinup);
                } else if (countAns == 3) {// || countAns == 3
                    mLlQue3.setVisibility(View.GONE);
                    mLlQue4.setVisibility(View.VISIBLE);
                    mLlQue4.startAnimation(animSlideinup);
                } else if (countAns == 4) {//||
                    mLlQue4.setVisibility(View.GONE);
                    mLlQue5.setVisibility(View.VISIBLE);
                    mLlQue5.startAnimation(animSlideinup);
                } else if (countAns == 5) {//|| countAns == 5
                    mLlQue5.setVisibility(View.GONE);
                    mLlQue6.setVisibility(View.VISIBLE);
                    mLlQue6.startAnimation(animSlideinup);
                } else if (countAns == 6) {//|| answer_click == true
                    mLlQue6.setVisibility(View.GONE);
                    mLlQue7.setVisibility(View.VISIBLE);
                    mLlQue7.startAnimation(animSlideinup);
                } else if (countAns == 7) {//|| answer_click == true
                    mLlQue7.setVisibility(View.GONE);
                    mLlQue8.setVisibility(View.VISIBLE);
                    mLlQue8.startAnimation(animSlideinup);
                } else if (countAns == 8) {//|| answer_click == true
                    mLlQue8.setVisibility(View.GONE);
                    mLlQue9.setVisibility(View.VISIBLE);
                    mLlQue9.startAnimation(animSlideinup);
                } else if (countAns == 9) {
                    mLlQue9.setVisibility(View.GONE);
                    mLlQue10.setVisibility(View.VISIBLE);
                    mLlQue10.startAnimation(animSlideinup);
                } else if (countAns == 10) {
                    Log.e("test", "completed-" + countAns);
                    Functions.ToastUtility(TestScreenSample.this, "Thank You");
                    btnNextquestion.setVisibility(View.GONE);
                    btnReAttempt.setVisibility(View.VISIBLE);
                }*/
            }
        });

        mIvBackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.no_animation);
            }
        });

        btnReAttempt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(TestScreenSample.this, TestScreenSample.class);

                intent.putExtra("txtsemsterID", semester_ID);
                intent.putExtra("txtstandardNO", "" + standard_NO);
                intent.putExtra("txtsubjectID", subject_ID);
                intent.putExtra("txtchapterID", chapter_ID);
                intent.putExtra("txtchapter_name", chapter_Name);
                intent.putExtra("txtsubject_name", subject_Name);

                Log.e("1_SendTestIntnt",
                        "semester_ID->" + semester_ID + " standard_NO->" + standard_NO +
                                " subject_ID->" + subject_ID + "  subject_Name->" + subject_Name +
                                " chapter_ID->" + chapter_ID + "  chapter_Name->" + chapter_Name);

                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.no_animation);


                //Log.e("CorrectAnswer ", "-> " + countAns);
/*                Intent intent = new Intent(TestScreenSample.this, ResultScreen.class);
                intent.putExtra("sendanswer", countAns);
                intent.putExtra("txtsemsterID", semester_ID);
                intent.putExtra("txtstandardNO", "" + standard_NO);
                intent.putExtra("txtsubjectID", subject_ID);
                intent.putExtra("txtchapterID", chapter_ID);
                intent.putExtra("txtchapter_name", chapter_Name);
                intent.putExtra("txtsubject_name", subject_Name);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_right, R.anim.no_animation);*/
            }
        });


        if (Functions.knowInternetOn(this)) {
            String std = getResources().getString(R.string._class);
            if (mFunction.getFlagLogCode() == 1) {
                //APIs.getQuiz(this, this, "2", "6", "3", "49");
                Log.e("Call API", "getFlagLogCode-1->" + mFunction.getFlagLogCode());
                mTvStandard.setText(std + " : " + mFunction.getStandardName());
                APIs.getChapterQuiz(this, this, mFunction.getPrefMediumId(), mFunction.getSection(),
                        mFunction.getSemester(), mFunction.getStandardId(), mFunction.getSubjectId(), mFunction.getChapterId());

            } else {
                Log.e("Call API", "getFlagLogCode-2->" + mFunction.getFlagLogCode());
                mTvStandard.setText(std + " : " + mFunction.getStandardName());
                APIs.getChapterQuiz(this, this, mFunction.getPrefMediumId(), mFunction.getSection(),
                        mFunction.getSemester(), mFunction.getStandardId(), mFunction.getSubjectId(), mFunction.getChapterId());
            }
        } else {
            Functions.showInternetAlert(this);
        }
    }


    @Override
    public void onResult(JSONObject jobjWhole) {
        try {
            if (jobjWhole != null) {
                JSONObject jObj = jobjWhole.optJSONObject(Connection.TAG_DATA);
                String strApiName = jObj.optString("api");
                int strStatus = jObj.optInt("success");
                String strMessage = jObj.optString("message");
                if (strApiName.equalsIgnoreCase("getChapterQuiz")) {
                    if (strStatus != 0) {
                        JSONArray jArrayTextSub = jObj.getJSONArray("quizData");
                        Log.e("arraylength", "" + jArrayTextSub.length());

                        if (jArrayTextSub.length() > 0) {
                            mTvNoQuiz.setVisibility(View.GONE);
                            mLlquestionGrp.setVisibility(View.VISIBLE);
                            btnNextquestion.setVisibility(View.VISIBLE);
                            mLlQue1.startAnimation(animSlideinup);


                            JSONObject objj_1 = jArrayTextSub.getJSONObject(0);
                            JSONObject objj_2 = jArrayTextSub.getJSONObject(1);
                            JSONObject objj_3 = jArrayTextSub.getJSONObject(2);
                            JSONObject objj_4 = jArrayTextSub.getJSONObject(3);
                            JSONObject objj_5 = jArrayTextSub.getJSONObject(4);
                            JSONObject objj_6 = jArrayTextSub.getJSONObject(5);
                            JSONObject objj_7 = jArrayTextSub.getJSONObject(6);
                            JSONObject objj_8 = jArrayTextSub.getJSONObject(7);
                            JSONObject objj_9 = jArrayTextSub.getJSONObject(8);
                            JSONObject objj_10 = jArrayTextSub.getJSONObject(9);


                            mTvquestion1.setText(objj_1.optString("question"));

                            mTvQuestion1_OptionA.setText(objj_1.optString("A"));
                            mTvQuestion1_OptionB.setText(objj_1.optString("B"));
                            mTvQuestion1_OptionC.setText(objj_1.optString("C"));
                            mTvQuestion1_OptionD.setText(objj_1.optString("D"));
                            correct_answer1 = objj_1.optString("correct_answer");
                            Log.e("answer", "correct_answer1-" + correct_answer1);

//

                            mTvquestion2.setText(objj_2.optString("question"));
                            mTvQuestion2_OptionA.setText(objj_2.optString("A"));
                            mTvQuestion2_OptionB.setText(objj_2.optString("B"));
                            mTvQuestion2_OptionC.setText(objj_2.optString("C"));
                            mTvQuestion2_OptionD.setText(objj_2.optString("D"));
                            correct_answer2 = objj_2.optString("correct_answer");
                            Log.e("answer", "correct_answer2-" + correct_answer2);


                            mTvquestion3.setText(objj_3.optString("question"));
                            mTvQuestion3_OptionA.setText(objj_3.optString("A"));
                            mTvQuestion3_OptionB.setText(objj_3.optString("B"));
                            mTvQuestion3_OptionC.setText(objj_3.optString("C"));
                            mTvQuestion3_OptionD.setText(objj_3.optString("D"));
                            correct_answer3 = objj_3.optString("correct_answer");
                            Log.e("answer", "correct_answer3-" + correct_answer3);

                            mTvquestion4.setText(objj_4.optString("question"));
                            mTvQuestion4_OptionA.setText(objj_4.optString("A"));
                            mTvQuestion4_OptionB.setText(objj_4.optString("B"));
                            mTvQuestion4_OptionC.setText(objj_4.optString("C"));
                            mTvQuestion4_OptionD.setText(objj_4.optString("D"));
                            correct_answer4 = objj_4.optString("correct_answer");
                            Log.e("answer", "correct_answer4-" + correct_answer4);


                            mTvquestion5.setText(objj_5.optString("question"));
                            mTvQuestion5_OptionA.setText(objj_5.optString("A"));
                            mTvQuestion5_OptionB.setText(objj_5.optString("B"));
                            mTvQuestion5_OptionC.setText(objj_5.optString("C"));
                            mTvQuestion5_OptionD.setText(objj_5.optString("D"));
                            correct_answer5 = objj_5.optString("correct_answer");
                            Log.e("answer", "correct_answer5-" + correct_answer5);


                            mTvquestion6.setText(objj_6.optString("question"));
                            mTvQuestion6_OptionA.setText(objj_6.optString("A"));
                            mTvQuestion6_OptionB.setText(objj_6.optString("B"));
                            mTvQuestion6_OptionC.setText(objj_6.optString("C"));
                            mTvQuestion6_OptionD.setText(objj_6.optString("D"));
                            correct_answer6 = objj_6.optString("correct_answer");
                            Log.e("answer", "correct_answer6-" + correct_answer6);


                            mTvquestion7.setText(objj_7.optString("question"));
                            mTvQuestion7_OptionA.setText(objj_7.optString("A"));
                            mTvQuestion7_OptionB.setText(objj_7.optString("B"));
                            mTvQuestion7_OptionC.setText(objj_7.optString("C"));
                            mTvQuestion7_OptionD.setText(objj_7.optString("D"));
                            correct_answer7 = objj_7.optString("correct_answer");
                            Log.e("answer", "correct_answer7-" + correct_answer7);


                            mTvquestion8.setText(objj_8.optString("question"));
                            mTvQuestion8_OptionA.setText(objj_8.optString("A"));
                            mTvQuestion8_OptionB.setText(objj_8.optString("B"));
                            mTvQuestion8_OptionC.setText(objj_8.optString("C"));
                            mTvQuestion8_OptionD.setText(objj_8.optString("D"));
                            correct_answer8 = objj_8.optString("correct_answer");
                            Log.e("answer", "correct_answer8-" + correct_answer8);

                            mTvquestion9.setText(objj_9.optString("question"));
                            mTvQuestion9_OptionA.setText(objj_9.optString("A"));
                            mTvQuestion9_OptionB.setText(objj_9.optString("B"));
                            mTvQuestion9_OptionC.setText(objj_9.optString("C"));
                            mTvQuestion9_OptionD.setText(objj_9.optString("D"));
                            correct_answer9 = objj_9.optString("correct_answer");
                            Log.e("answer", "correct_answer9-" + correct_answer9);

                            mTvquestion10.setText(objj_10.optString("question"));
                            mTvQuestion10_OptionA.setText(objj_10.optString("A"));
                            mTvQuestion10_OptionB.setText(objj_10.optString("B"));
                            mTvQuestion10_OptionC.setText(objj_10.optString("C"));
                            mTvQuestion10_OptionD.setText(objj_10.optString("D"));
                            correct_answer10 = objj_10.optString("correct_answer");
                            Log.e("answer", "correct_answer10-" + correct_answer10);


                            /*mFunction.SetPrefQuiz(objj_1.optString("question"), objj_1.optString("correct_answer"),
                                    objj_2.optString("question"), objj_2.optString("correct_answer"),
                                    objj_3.optString("question"), objj_3.optString("correct_answer"),
                                    objj_4.optString("question"), objj_4.optString("correct_answer"),
                                    objj_5.optString("question"), objj_5.optString("correct_answer"),
                                    objj_6.optString("question"), objj_6.optString("correct_answer"),
                                    objj_7.optString("question"), objj_7.optString("correct_answer"),
                                    objj_8.optString("question"), objj_8.optString("correct_answer"),
                                    objj_9.optString("question"), objj_9.optString("correct_answer"),
                                    objj_10.optString("question"), objj_10.optString("correct_answer"));*/

                        } else {
                            mTvNoQuiz.setVisibility(View.VISIBLE);
                            mLlquestionGrp.setVisibility(View.INVISIBLE);
                            btnNextquestion.setText(View.GONE);
                        }
                    } else {
                        Functions.ToastUtility(TestScreenSample.this, strMessage);
                        mTvNoQuiz.setVisibility(View.VISIBLE);
                        mLlquestionGrp.setVisibility(View.INVISIBLE);
                        btnNextquestion.setText(View.GONE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}